<?php

/**
 * @file
 * Administrative page callbacks for the privy module.
 */

/**
 * Implements hook_admin_settings().
 */
function privy_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['privy_identifier'] = array(
    '#title' => t('Account Identifier'),
    '#type' => 'textfield',
    '#default_value' => variable_get('privy_identifier', ''),
    '#size' => 20,
    '#maxlength' => 30,
    '#required' => TRUE,
    '#description' => t('Your Privy account identifier. Go to <a href="@dashboard">Privy dashboard</a> to get it.', array('@dashboard' => 'http://dashboard.privy.com')),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function privy_admin_settings_form_validate($form, &$form_state) {
}
