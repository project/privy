<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function privy_variable_info($options) {
  $variables['privy_identifier'] = array(
    'type' => 'string',
    'title' => t('Account Identifier', array(), $options),
    'default' => 'UA-',
    'description' => t('Your Privy account identifier. Go to <a href="@dashboard">Privy dashboard</a> to get it.', array('@dashboard' => 'http://dashboard.privy.com'), $options),
    'required' => TRUE,
    'group' => 'privy',
    'localize' => TRUE,
    'multidomain' => TRUE,
    'validate callback' => 'privy_validate_privy_account',
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function privy_variable_group_info() {
  $groups['privy'] = array(
    'title' => t('Privy'),
    'description' => t('Configure Privy.'),
    'access callback' => 'user_access',
    'access arguments' => array('administer access control'),
    'path' => array('admin/config/system/privy'),
  );

  return $groups;
}

/**
 * Validate privy identifier.
 */
function privy_validate_privy_account($variable) {

  if (!preg_match('/^[a-z0-9]+$/', $variable['value'])) {
    return t('Please enter a valid Privy identifier.');
  }
}
