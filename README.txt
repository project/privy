
Module: Privy
Author: Privy, inc. <http://drupal.org/user/85918>


Description
===========
Adds the Privy code to your website.

-- REQUIREMENTS --

* Privy.com account - sign up for free at http://dashboard.privy.com


-- INSTALLATION --
Copy the 'privy' module directory in to your Drupal
sites/all/modules directory as usual.


-- CONFIGURATION --

Visit the settings page at: Configuration » System » Privy

In the settings page, enter your Privy account identifier.
